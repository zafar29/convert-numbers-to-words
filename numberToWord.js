const numToWord = {
    0:'zero', 1:'one', 2:'two', 3:'three',  4:'four',  5:'five',  6:'six', 7:'seven', 8:'eight', 9:'nine', 10:'ten',
    11:'eleven', 12:'twelve', 13:'thirteen', 14:'fourteen', 15:'fifteen', 16:'sixteen', 17:'seventeen', 18:'eighteen', 19:'nineteen',
    20:'twenty', 30:'thirty', 40:'fourty', 50:'fifty', 60:'sixty', 70:'seventy', 80:'eighty', 90:'ninety', 100:'hundred', 1000: 'One thousand'
};

function numberToWord(number)

{if(number<21)
{
    return numToWord[number];
}
}
console.log(numberToWord(20));

var count = 0;

while (count < 1001) {
    let word = number2Word(count);
    if(word) {
        console.log(word);
    }
    else {
        console.log('word for '+count+' not found.');
    }
    count++;
}

function number2Word(num) {
    if(num > 1000) {
        return false;
    }

    if(typeof numToWord[num] != 'undefined') {
        return numToWord[num];
    }
    
    let strNumber = num.toString().split('').reverse();
    
    let word = '';
    
    if(strNumber[0] !== '0' && typeof strNumber[0] != 'undefined') {
        word =  numToWord[strNumber[0]];   	
    }
    
    if(strNumber[1] !== '0' && typeof strNumber[1] != 'undefined') {
        word =  numToWord[strNumber[1]*10]+' '+word;
    }
    if(typeof numToWord[strNumber[1].concat(strNumber[0])] != 'undefined') {
        word = numToWord[strNumber[1].concat(strNumber[0])];
    }
    
    if(typeof strNumber[2] != 'undefined') {
        word =  numToWord[strNumber[2]]+' hundred '+word;
    }

    return word;
}